// const logger = require('@pubsweet/logger')
const fs = require("fs-extra");
const epubchecker = require("epubchecker");

const validationHandler = async (req, res) => {
  try {
    if (req.fileValidationError) {
      return res.status(400).json({ msg: req.fileValidationError });
    }
    if (!req.file) {
      return res.status(400).json({ msg: "EPUB file is not included" });
    }
    const { path: filePath } = req.file;
    console.log("whaa", filePath);
    const report = await epubchecker(filePath, {
      includeWarnings: false,
      // do not check CSS and font files
      exclude: /\.(css|ttf|opf|woff|woff2)$/,
    });

    const {
      checker: { nError },
      messages,
    } = report;

    await fs.remove(filePath);
    return res.status(200).json({
      outcome: "ok",
      errors: messages,
    });
  } catch (e) {
    throw new Error(e);
  }
};

module.exports = { validationHandler };
