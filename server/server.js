const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");
const config = require("config");

const logger = config.get("server.logger");
const serverPort = config.get("server.port") || 9000;
const { uploadHandler } = require("./utils/utils");
const { validationHandler } = require("./routes/validator.controller");

logger.info("initializing the server");
try {
  const app = express();

  app.use(bodyParser.json({ limit: "20mb" }));
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(
    helmet({
      contentSecurityPolicy: false, //this is the problematic policy which causing issues when servicing the index.html
    })
  );
  app.use(
    cors({
      origin: "http://localhost:3000",
    })
  );
  app.use(express.static(path.join(__dirname, "public")));
  app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, "public", "index.html"));
  });
  app.post("/api/validator", uploadHandler, validationHandler);

  logger.info("starting the server");

  app.listen(serverPort, () => {
    logger.info(`server is up and listening on port ${serverPort}`);
  });
} catch (e) {
  logger.error(e.message);
  throw new Error(e);
}
