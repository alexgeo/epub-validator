const path = require("path");
const { raw } = require("config/raw");

const winston = require("winston");
const DailyRotateFile = require("winston-daily-rotate-file");

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      format: winston.format.simple(),
    }),
    new DailyRotateFile({
      filename: "%DATE%.log",
      dirname: path.join(__dirname, "../logs"),
      datePattern: "YYYY-MM-DD",
      zippedArchive: true,
      maxFiles: "30d",
      json: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }),
  ],
});

module.exports = {
  server: {
    logger: raw(logger),
    port: 3000,
  },
};
