const { raw } = require("config/raw");
const winston = require("winston");

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      format: winston.format.simple(),
    }),
  ],
});

module.exports = {
  server: {
    logger: raw(logger),
    port: 3000,
  },
};
