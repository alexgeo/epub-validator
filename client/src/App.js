import styled from "styled-components";
import MainSection from "./elements/MainArea";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;
const HeaderWrapper = styled.header`
  align-items: center;
  display: flex;
  flex-grow: 1;
  justify-content: center;
  height: 10%;
`;
const Header = styled.h1`
  color: #195698;
`;
function App() {
  return (
    <Wrapper>
      <HeaderWrapper>
        <Header>EPUB Validator</Header>
      </HeaderWrapper>
      <MainSection />
    </Wrapper>
  );
}

export default App;
