import React, { Fragment } from "react";
import styled from "styled-components";
import axios from "axios";
// Style the Button component
const Button = styled.button`
  /* Insert your favorite CSS code to style a button */
  width: 20%;
  height: 50px;
  align-self: center;
`;
const FileUploader = (props) => {
  // Create a reference to the hidden file input element
  const hiddenFileInput = React.useRef(null);

  // Programatically click the hidden file input element
  // when the Button component is clicked
  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  // Call a function (passed as a prop from the parent component)
  // to handle the user-selected file
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    const bodyFormData = new FormData();
    bodyFormData.append("epub", fileUploaded);
    axios({
      method: "post",
      url: "http://localhost:3000/api/validator",
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(function (response) {
        //handle success
        console.log(response);
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  };
  return (
    <Fragment>
      <Button onClick={handleClick}>Upload a file</Button>
      <input
        type="file"
        ref={hiddenFileInput}
        onChange={handleChange}
        style={{ display: "none" }}
      />
    </Fragment>
  );
};
export default FileUploader;
