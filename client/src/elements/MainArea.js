import styled from "styled-components";
import FileUploader from "./FileUploader";

const MainSectionWrapper = styled.section`
  display: flex;
  flex-direction: column;
  margin-top: 1%;
  padding: 2%;
  height: 90%;
  width: 96%;
`;
const Paragraph = styled.p`
  width: 100%;
  margin-top: 1%;
  margin-bottom: 3%;
`;

const MainSection = (props) => {
  return (
    <MainSectionWrapper>
      <Paragraph>
        Submit an EPUB document for validation. Your file must be 20MB or less.
      </Paragraph>
      <FileUploader />
    </MainSectionWrapper>
  );
};

export default MainSection;
