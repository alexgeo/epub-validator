
FROM node:12-alpine AS ui-build
WORKDIR /home/client

COPY ./client .

RUN yarn

RUN yarn build

FROM node:12-alpine

RUN apk update && apk add --no-cache unzip openjdk11-jre-headless git

WORKDIR /home/node/validator


COPY ./server/package.json ./package.json
COPY ./server/yarn.lock ./yarn.lock

RUN yarn

COPY ./server .
COPY --from=ui-build /home/client/build ./public

RUN chown -R node:node /home/node/validator

USER node

ENTRYPOINT node ./server.js
